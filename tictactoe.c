#include <ncurses.h>

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define CONTINUE 0
#define DRAW 3

int * cords(int i);
void drawBoard(char board[], int scores[]);
void printTurn(int turn);
bool input(char board[], int turn, char *play, int *win);
int checkForWin(char board[], int turn);

int main(){
    /* Start curses mode */
    initscr();
    curs_set(0);
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_BLUE, COLOR_BLACK);
    time_t t;
    srand((unsigned) time(&t));
    int turn = rand() % 2 + 1;
    int scores[] = {0,0,0};
    char play = 'y';
    while(play != '\e'){
        char board[] = {'0','1','2','3','4','5','6','7','8','9'};
        //Change Turn to whoever is losing, or just evened out
        if(scores[1] != scores[2]){
            turn = (scores[1] < scores[2]) ? 1 : 2;
        }
        int win = checkForWin(board, turn);
        while(win == CONTINUE){
            drawBoard(board, scores);
            printTurn(turn);
            bool validInput = input(board, turn, &play, &win);
            while(validInput == false){    
                drawBoard(board, scores);
                printTurn(turn);
                mvwprintw(stdscr, 17, 0,"\tInvalid input!\n");
                validInput = input(board, turn, &play, &win);
            }
            if(win == CONTINUE){
                win = checkForWin(board,turn);
            }
            if(win == CONTINUE){
                //alternating turns
                turn = turn % 2 + 1;
            }
        }
        if((win == 1) || (win == 2)){
            scores[win]++;
            drawBoard(board, scores);
            attron(COLOR_PAIR(win));
            mvwprintw(stdscr, 15, 1,"Player %d ( %c ) won this turn!\n", win, win==1 ? 'X' : 'O');
            attroff(COLOR_PAIR(win));
        }
        if(win == DRAW){
            drawBoard(board, scores);
            mvwprintw(stdscr, 15, 11,"Draw Game!\n");
        }
        if(play == 'y'){
            mvwprintw(stdscr, 16, 10,"Play again?\n");        
            mvwprintw(stdscr, 17, 6,"Press [ESC] to quit\n");        
            char ans = getch();
            if(ans == '\e'){
                play = ans;
            }
        }
    }
    int winner;
    if(scores[1] == scores[2]){
        winner = DRAW;
    }
    else{
        winner = (scores[1] > scores[2]) ? 1 : 2;
    }
    clear();
    if(winner != DRAW){
        attron(COLOR_PAIR(winner));
        mvwprintw(stdscr,6,5,"Player %d ( %c ) won!\n", winner, winner == 1 ? 'X':'O');
        attroff(COLOR_PAIR(winner));
    }
    else{
        mvwprintw(stdscr,6,10,"Draw Game!\n");
    }
    mvwprintw(stdscr,7,8,"Scores: %i : %i\n", scores[1],scores[2]);
    getch();
    endwin();
    return 0;
}

void drawBoard(char board[], int scores[]){
    clear();
    char *boardStr;
    boardStr =  "[ESC]     Tic Tac Toe\n\n"
                "Player 1 (   ) - Player 2 (   )\n"
                "               :        \n\n"
                "            |     |     \n"
                "            |     |     \n"
                "       _____|_____|_____\n"
                "            |     |     \n"
                "            |     |     \n"
                "       _____|_____|_____\n"
                "            |     |     \n"
                "            |     |     \n"
                "            |     |     \n\n";
    printw(boardStr);
    attron(COLOR_PAIR(1)); //print 'X' and score in header
    mvwprintw(stdscr, 2, 11, "%c", 'X');
    mvwprintw(stdscr, 3, 0, "%8d", scores[1]);
    attroff(COLOR_PAIR(1));
    attron(COLOR_PAIR(2)); //print 'O' and score in header
    mvwprintw(stdscr, 2, 28, "%c", 'O');
    mvwprintw(stdscr, 3, 24, "%d", scores[2]);
    attroff(COLOR_PAIR(2));
    for (int i = 1; i < 10; i++){
        int *c = cords(i);
        if(board[i] == 'X'){
            attron(COLOR_PAIR(1));
            mvwprintw(stdscr, c[0], c[1], "%c", board[i]);
            attroff(COLOR_PAIR(1));
        }else if(board[i] == 'O'){
            attron(COLOR_PAIR(2));
            mvwprintw(stdscr, c[0], c[1], "%c", board[i]);
            attroff(COLOR_PAIR(2));
        }else{
            mvwprintw(stdscr, c[0], c[1], "%c", board[i]);
        }
    mvwprintw(stdscr, 15, 0, "");
    }
}

int * cords(int i){
    static int c[2];
    c[0] = 12 - ( (i-1) / 3 ) * 3;
    c[1] = 9 + ( (i-1) % 3 ) * 6;
    return c; 
}

void printTurn(int turn){
    attron(COLOR_PAIR(turn));
    mvwprintw(stdscr, 15, 5,"Player %d ( %c )'s turn:\n", turn, turn == 1 ? 'X':'O');
    attroff(COLOR_PAIR(turn));
}

bool input(char board[], int turn, char *play, int *win){
    mvwprintw(stdscr, 16, 6,"Press [-] to give up\n");
    bool result = false;
    int place = getch();
    switch(place){
        case 'j':
            place = '1';
            break;
        case 'k':
            place = '2';
            break;
        case 'l':
            place = '3';
            break;
        case 'u':
            place = '4';
            break;
        case 'i':
            place = '5';
            break;
        case 'o':
            place = '6';
            break;
        default:
            break;
    }
    place -= '0';
    if( (place > 0) && (place < 10) ){
        if((board[place] != 'X') && ( board[place] != 'O')){
            board[place] = (turn == 1) ? 'X' : 'O';
            result = true;
        }
    } else if (place == '-' - '0'){
        *win = turn % 2 + 1;
        result = true;
    } else if (place == '\e' - '0') {
        *win = DRAW;
        *play = '\e';
        result = true;
    }
    return result;
}

int checkForWin(char board[], int turn){
    int result = DRAW;
    for(int i = 1;i <= 9;i++){
        if( (board[i] != 'X') && (board[i] != 'O') ){
            result = CONTINUE;
            break;
        }
    }
    for(int i = 1; i <= 7; i += 3){ 
        if((board[i] == board[i+1]) && (board[i] == board[i+2])){
            result = turn;
            break;
        }
    }
    for(int i = 1; i <= 3; i++){
        if ((board[i] == board[i+3]) && (board[i] == board[i+6])){
            result = turn;
            break;
        }
    }
    if((board[1] == board[5]) && (board[1] == board[9])){
        result = turn;
    }
    if((board[3] == board[5]) && (board[3] == board[7])){
        result = turn;
    }
    return result;
}