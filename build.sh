#!/usr/bin/env bash
echo "Installing libraries..."
echo ""
sudo apt install gcc
sudo apt install libncurses5-dev libncursesw5-dev -y
echo ""
echo "Compiling and linking..."
gcc tictactoe.c -o tictactoe -lncurses
echo "Done."